<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/admin/', function(){
   if(Auth::guest())
   {
     return Redirect::to('admin');
   }
   if(Auth::check()){
     return Redirect::to('dashboard');
   }
});*/

Auth::routes();


Route::prefix('admin')->group(function(){
    Route::get('/', 'Auth\AdminLoginController@showLoginForm')->name('admin');
    Route::post('/', 'Auth\AdminLoginController@login')->name('admin.login');
    Route::match(['get','post'],'/dashboard', 'AdminController@index')->name('admindashboard');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('adminlogout');
    Route::get('/profile', 'AdminController@profile')->name('profile');
    Route::post('/profile', 'AdminProfilesController@update')->name('adminprofileupdate')->middleware('auth:admin');
});


Route::prefix('user')->group(function(){
    Route::get('/', 'UserController@showLoginForm')->name('user');
    Route::post('/', 'UserController@login')->name('user.login');
    Route::match(['get','post'],'/dashboard', 'UserController@index')->name('dashboard');
    Route::get('/logout', 'UserController@logout')->name('userlogout');
    
});
 