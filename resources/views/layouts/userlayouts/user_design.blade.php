<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/assets/images/favicon.png')}}">
    <title>Elite User Template - The Ultimate Multipurpose admin template</title>
    <!-- This page CSS -->
    <!-- chartist CSS -->
    <link href="{{ asset('public/assets/node_modules/morrisjs/morris.css')}}" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="{{ asset('public/assets/node_modules/toast-master/css/jquery.toast.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('public/dist/css/style.min.css')}}" rel="stylesheet">
    <!-- Dashboard 1 Page CSS -->
    <link href="{{ asset('public/dist/css/pages/dashboard1.css')}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="skin-default fixed-layout">

@include('layouts.userlayouts.user_header')

@include('layouts.userlayouts.user_sidebar')

@yield('content')



@include('layouts.userlayouts.user_footer')



<script src="{{ asset('public/assets/node_modules/jquery/jquery-3.2.1.min.js')}}"></script>
    <!-- Bootstrap popper Core JavaScript -->
    <script src="{{ asset('public/assets/node_modules/popper/popper.min.js')}}"></script>
    <script src="{{ asset('public/assets/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('public/dist/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('public/dist/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset ('public/dist/js/sidebarmenu.j')}}s"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('public/dist/js/custom.min.js')}}"></script>
    <script src="{{ asset('public/assets/node_modules/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--morris JavaScript -->
    <script src="{{ asset('public/assets/node_modules/raphael/raphael-min.js')}}"></script>
    <script src="{{ asset('public/assets/node_modules/morrisjs/morris.min.js')}}"></script>
    <script src="{{ asset('public/assets/node_modules/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
    <!-- Popup message jquery -->
    <script src="{{ asset('public/assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
    <!-- Chart JS -->
    <script src="{{ asset('public/dist/js/dashboard1.js')}}"></script>
    <script src="{{ asset('public/assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
</body>

</html>