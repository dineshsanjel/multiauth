@extends('layouts.adminlayouts.admin_design')

@section('content')
<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Profile</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Profile</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <center class="m-t-30"> <img src="{{ asset('public/uploads/images/'.$admin->adminprofile->image)}}" class="img-circle" width="150" height="150" />
                                    <h4 class="card-title m-t-10">{{$admin->name}}</h4>
                                    <h6 class="card-subtitle">{{$admin->adminprofile->designation}}</h6>
                                    <div class="row text-center justify-content-md-center">
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i> <font class="font-medium">254</font></a></div>
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-picture"></i> <font class="font-medium">54</font></a></div>
                                    </div>
                                </center>
                            </div>
                            <div>
                                <hr> </div>
                            <div class="card-body"> <small class="text-muted">Email address </small>
                                <h6>{{$admin->email}}</h6> <small class="text-muted p-t-30 db">Phone</small>
                                <h6>{{$admin->adminprofile->mobile}}</h6> <small class="text-muted p-t-30 db">Address</small>
                                <h6>{{$admin->adminprofile->address}}</h6>
                                <div class="map-box">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d470029.1604841957!2d72.29955005258641!3d23.019996818380896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e848aba5bd449%3A0x4fcedd11614f6516!2sAhmedabad%2C+Gujarat!5e0!3m2!1sen!2sin!4v1493204785508" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div> <small class="text-muted p-t-30 db">Social Profile</small>
                                <br/>
                               <a href="{{$admin->adminprofile->facebook}}" target="_blank"> <button  class="btn btn-circle btn-secondary"><i class="fa fa-facebook"></i></button></a>

                               <a href="{{$admin->adminprofile->twitter}}" target="_blank">  <button class="btn btn-circle btn-secondary"><i class="fa fa-twitter"></i></button></a>

                                <a href="{{$admin->adminprofile->instagram}}" target="_blank"> <button class="btn btn-circle btn-secondary"><i class="fa fa-instagram"></i></button></a>

                             <a href="{{$admin->adminprofile->linkedin}}" target="_blank"> <button class="btn btn-circle btn-secondary"><i class="fa fa-linkedin"></i></button></a>
                            </div>
                           
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Profile</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Settings</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                               <!--second tab-->
                                <div class="tab-pane active" id="profile" role="tabpanel">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Full Name</strong>
                                                <br>
                                                <p class="text-muted">{{$admin->name}}</p>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong>
                                                <br>
                                                <p class="text-muted">{{$admin->adminprofile->mobile}}</p>
                                            </div>
                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                                <br>
                                                <p class="text-muted">{{$admin->email}}</p>
                                            </div>
                                            <div class="col-md-3 col-xs-6"> <strong>Location</strong>
                                                <br> 
                                                <p class="text-muted">{{$admin->adminprofile->address}}</p>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                                <div class="tab-pane" id="settings" role="tabpanel">
                                    <div class="card-body">
                                        <form class="form-horizontal form-material" action="{{route('adminprofileupdate')}}" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group">

                                                <label>Featured Image</label>
                                 <div class="controls">
                            <input type="file" name="image" id="image">
                            <input type="hidden" name="current_image" value="{{ $admin->adminprofile->image }}">
                            @if(!empty($admin->adminprofile->image))
                            <img style="width: 40px" src="{{ asset('public/uploads/images/'.$admin->adminprofile->image)}}"> |
                          @endif
                          </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Full Name</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="name" value="{{$admin->name}}" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="email" class="col-md-12">Email</label>
                                                <div class="col-md-12">
                                                    <input type="email"  class="form-control form-control-line" name="email" id="email" value="{{$admin->email}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Password</label>
                                                <div class="col-md-12">
                                                    <input type="password" name="password" value="{{$admin->password}}" class="form-control form-control-line">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-12">Designation</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="designation" value="{{$admin->adminprofile->designation}}" class="form-control form-control-line">
                                                </div>
                                            </div>
                                           
                                            <div class="form-group">
                                                <label class="col-md-12">Phone No</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="mobile"  value="{{$admin->adminprofile->mobile}}" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Address</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="address"  value="{{$admin->adminprofile->address}}" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">About Me</label>
                                                <div class="col-md-12">
                                                    <textarea rows="5" name="about" class="form-control form-control-line">{!! $admin->adminprofile->about !!}</textarea>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-sm-12">Select Country</label>
                                                <div class="col-sm-12">
                                                	
                                                    <select name="country" class="form-control form-control-line" >
                                                    	@foreach($countries as $country)
                                                        <option   value="{{$country->country_name}}">{{$country->country_name}}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-md-12">Facebbok</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="facebook" value="{{$admin->adminprofile->facebook}}" class="form-control form-control-line">
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-md-12">Twitter</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="twitter" value="{{$admin->adminprofile->twitter}}" class="form-control form-control-line">
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-md-12">Instagram</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="instagram" value="{{$admin->adminprofile->instagram}}" class="form-control form-control-line">
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-md-12">Linkedin</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="linkedin" value="{{$admin->adminprofile->linkedin}}" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-success">Update Profile</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>



@endsection