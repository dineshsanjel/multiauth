<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\support\Facades\Input;
use App\ImageModel;
use App\AdminProfile;
use App\Admin;
use Image;
use Auth;

class AdminProfilesController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth:admin');
  }

  public function update(Request $request)
  {
 
    $admin = Auth::user('admin');
  /*  echo "<pre>"; print_r($request->all()); die();*/
       if ($request->hasFile('image')){
   /* echo "<pre>"; print_r($request->all()); die();*/

                $image_tmp = Input::file('image');
                if ($image_tmp->isValid()) {
                    //echo "test";die;
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(111,99999).'.'.$extension;
                    $large_image_path = 'public/uploads/images/'.$filename;
                    
                    Image::make($image_tmp)->save($large_image_path);
                   
                    $admin->adminprofile->image = $filename;
                }

            }
            else
            {
                $admin->adminprofile->image = $request->current_image;
            }

    $admin->name = $request->name;
    $admin->email = $request->email;
    $admin->adminprofile->mobile = $request->mobile;
    $admin->adminprofile->about = $request->about;
    $admin->adminprofile->linkedin = $request->linkedin;
    $admin->adminprofile->twitter = $request->twitter;
    $admin->adminprofile->instagram = $request->instagram;
    $admin->adminprofile->country = $request->country;
    $admin->adminprofile->address = $request->address;
    $admin->adminprofile->designation = $request->designation;
    $admin->adminprofile->facebook = $request->facebook;
    
     if($request->has('password')){
      $admin->password = bcrypt($request->password);
   
    }

    $admin->save();
    $admin->adminprofile->save();
   
    return redirect()->back();
  
}
}
