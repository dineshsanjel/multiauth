<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\AdminProfile;
use App\Admin;
use Auth;


class AdminController extends Controller
{
  
       public function __construct()
    {
        $this->middleware('auth:admin');
    }



 public function index(){
  
   return view('admin.dashboard');
 }

 public function profile(){
  
  $countries = DB::table('countries')->get();
  return view ('admin.profile')->with('admin', Auth::user('admin'))->with('countries', $countries);
 }


}
