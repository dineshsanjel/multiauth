<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;



class UserController extends Controller
{
   
  
   public function showLoginForm()
   {
     return view ('user.user_login');
   }
   

   public function login(Request $request){
     $this->validate($request, [
         'email' => 'required|email',
         'password' => 'required|min:6'
     ]);

     if(Auth::guard()->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)){

       return redirect()->intended(route('dashboard'));
     }
     return redirect()->back()->withInput($request->only('email', 'remember'));
   }


   public function index(){
  
   return view('user.dashboard');
 }

   public function logout()
   {
       Auth::guard()->logout();
       return redirect('/user');
   }
}